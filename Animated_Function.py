from __future__ import division
import random

import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style

style.use('fivethirtyeight')


def gen_dict_w_values(list_len=20, dict_size=10):
    """
    Generates a dicitonary of values with the next entry each value being 0.5 higher that the previous one.
    :param list_len: Length of the list. Default 20
    :param dict_size: Size of the dictionary. Default 10
    :return: Dictionary containing float values.
    """
    lst = []
    dct = {}
    for q in range(list_len):
        numb = random.randint(0, 10)
        lst.append(numb)

    for q in range(dict_size):
        dct[q] = lst
        lst = [x+0.5 for x in lst]

    return dct

dct = gen_dict_w_values()

for q in dct:
    y_max = max(dct[q])
    x_max = len(dct[q])
print "ymax", y_max
print "xmax", x_max


fig = plt.figure()

plt.axis([0, x_max, 0, y_max])

ax1 = fig.add_subplot(1,1,1)

print dct


def data_gen(k=0):
    """
    Defining the x and y for the automated graph plotting.
    :param k: Counter, that is changed to 1 when the full cycle of plotting.
    :return: Yeilds defined x, y and y values
    """
    while k < len(dct):
        xs = range(0, len(dct[k]))
        ys = dct[k]
        k += 1
        print k
        yield xs, ys, k


def animate(data):
    """
    Defines the plots.
    :param data:    The k value, or the counter indicating full cycle closure (if 1).
    :return:        Function with defined values for animated plotting.
    """
    xs, ys, k = data

    if k == 1:
        ax1.clear()
        ax1.plot(xs, ys)
    else:
        ax1.plot(xs, ys)

    return ax1


ani = animation.FuncAnimation(fig, animate, data_gen, interval=1000)
plt.show()

