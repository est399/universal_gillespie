# Universal Gillespie Algorithm README #

#### Version 0.1.0

### Description

The following repository contains scripts that were implemented in development of universal Gillespie algorithm, as well as Gillespie Universal algorithm test unit. 
Gillespie Universal algorithm generates a statistically correct trajectory for stochastic equation based of biochemical reactions specified in **reaction_data.txt** file. A graph is constructed.

### System Requirements
* Python 2.7

### Software Requirements
* matplotlib
* numpy

#### 1. Animated_function.py
> This programme generates a random list of numbers from 0 to 10 and writes it to a dictionary. Every next entry into the dictionary is same list with values being 0.25 higher than in the previous one. By default list length is 20 and dictionary size is 10. Further **matplotlit.pyplot** library was used to create plots and **matplotlib.animation** implemented in animation creation. Style was definition was imported using **matplotlib.style**.
>
> Function can be called from the command line using the `python2.7 Animated_Function.py` command.

#### 2. Gillespie.py
> A simple implementation of Gillespie algorithm for a bidirectional biochemical reaction A + B <-> AB. Based on deterministic reaction rate equation and Monte Carlo method stochasticity is introduced through calculation of propensity of every species in the system and employing random uniform distribution picking one of the possible reactions as the next one occurring. Furthermore, the time for the next instance is calculated from exponential distribution with *lambda* being equal to the sum of propensities. By default the reaction will run for 100 cycles. For the simplification of calculations **__future__** was imported from **division**.
>
> Function can be called from the command line using the `python2.7 Gillespie.py` command.

#### 3. Gillespie_Universal.py
> A more complex implementation of Gillespie algorithm which allows calculation of species and further graph production for any kind of biochemical reaction up to 3rd order. Addition libraries include: **__future__** from **division**; **random**; **numpy**; **sys**; and **matplotlib.pyplot**.
>
> For running the algorithm command line command `python2.7 Gillespie_Universal.py` can be called. As additional argument file 'reaction_data.txt' can be used. The file contains specifications for the programme to run, including specification of the max y and x values, initial state of the species and the biochemical reactions data in a form of a table (matrix) with change in each species represented by either *+1/-1; 0; +2/-2*.

#### 4. test.py
> Implementing **unittest** library a unit test for major functions within the Gillespie_Universal.py was written. In order to run the test unit command `python2.7 test.py` should be called.