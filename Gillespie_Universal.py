from __future__ import division
import random
import numpy as np
import sys
import matplotlib.pyplot as plt


class GillespieAdvanced:

    time_list = [0]

    def __init__(self):

        self.state = np.matrix('[100,  100,  100,  0,   0]')

        self.react_mat = np.matrix('0,  0,  -1, 0,  0,  5;'
                                   '-1, -1, +1, 0,  0,  800;'
                                   '0,  0,  -1, 2,  0,  125;'
                                   '2,  0,  0,  -1, 0,  200;'
                                   '0,  2,  0,  -1, 0,  175;'
                                   '0,  0,  0,  -2, 1,  100')

        self.max_time_x = 50
        self.max_species_y = 100

    def __str__(self):
        print "Initial Concentrations were {}".format(self.state)
        print "Reactions Matrix {}".format(self.react_mat)

    def gen_h_matrix(self):
        """
        Generates a matrix with entries corresponding to the type of reaction thus representing
        number of particular changes in each reaction.

        - cp1/2 and cm1/2 stand for count_plus and count_minus -> determine the counter for additions and subtractions.
        - order is the name of the matrix while it is being generated
        :param:    matrix defining reactions -> containing changes in quantity of reagents
        :return:   matrix defining h
        """
        reactions_matrix = self.react_mat
        cp1, cm1, cp2, cm2 = 0, 0, 0, 0             # Counter with addition/subtraction -> count plus, or count minus

        order = np.matrix([cp1, cm1, cp2, cm2])     # Creating a matrix to determine the order of the reaction

        for q in range(len(reactions_matrix)):
            cp1, cm1, cp2, cm2 = 0, 0, 0, 0

            for e in range(np.size(reactions_matrix[q])):
                if reactions_matrix[q, e] == 1:
                    cp1 += 1
                elif reactions_matrix[q, e] == -1:
                    cm1 += 1
                elif reactions_matrix[q, e] == 2:
                    cp2 += 1
                elif reactions_matrix[q, e] == -2:
                    cm2 += 1
            order = np.vstack([order, [cp1, cm1, cp2, cm2]])
        order = np.delete(order, 0, axis=0)         # Delete the first row that was used to create matrix
        return order

    def gen_h_list(self, h_mtrx):

        """
        Generates a list contain containing type of reaction (h), with every new entry representing each reaction.
        :param h_mtrx:  matrix defining h -> containing number of particular changes in each reaction
        :return:        list

        """
        h = [0]*np.size(h_mtrx, axis = 0)
        for q in range(len(h_mtrx)):
            if np.matrix.tolist(h_mtrx)[q] == [1, 0, 0, 0] or \
                    np.matrix.tolist(h_mtrx)[q] == [0, 1, 0, 0]:
                h[q] = 1
            elif np.matrix.tolist(h_mtrx)[q] == [1, 1, 0, 0] or \
                    np.matrix.tolist(h_mtrx)[q] == [0, 1, 1, 0]:
                h[q] = 2    # nA
            elif np.matrix.tolist(h_mtrx)[q] == [1, 2, 0, 0]:
                h[q] = 3    # nA * nB
            elif np.matrix.tolist(h_mtrx)[q] == [1, 0, 0, 1]:
                h[q] = 4    # nA(nA - 1)/2
        return h

    def reag_numbers(self):
        """
        Defines participating elements, by taking in the matrix with reactions and returns a dictionary with keys
        indicating the number of the reactions
        and the values representing number of the reagent within the dictionary
        :return: dictionary containing the reactions index
        """
        react_mat = self.react_mat

        dic = {}
        for q in range(len(react_mat)):
            lst = []
            for e in range(react_mat.shape[1] - 1):
                if react_mat[q, e] < 0:
                    lst.append(e)
                dic[q] = lst
        return dic

    def test_if_reaction_possible(self):
        """
        Tests which reactions are possible by performing subtraction from the
        reagents number matrix the reagent change matrix.
        (note: commented out is the new concentrations after the reaction have occurred for all of the reactions)
        :return: list with number of reactions
        """
        state = self.state
        react_mat = self.react_mat

        pos_react = []

        for q in range(len(react_mat)):
            y = state + react_mat[q, :-1]
            if np.all((y >= 0), axis=1):
                pos_react.append(q)

        if pos_react == []:
            raise NameError('No reactions are possible.')
        else:
            return pos_react

    def calculate_propensity(self, type_h, pos_react, reag_numb):
        """
        Calculates propensity for each of the reaction.
        :param type_h:      List containing types of reactions with position corresponding to the reaction index.
        :param pos_react:   List containing index of possible reaction.
        :param reag_numb:   Dictionary with keys corresponding to reaction index and values containing index of the
                            reagents (species) participating in the reaction.
        :return:            Dictionary with keys corresponding to the possible reactions and values to the individual
                            propensity values. Additional entry 'sum' contains sum of propensities.
        """
        react_mat = self.react_mat
        state = self.state
        prop_dict = {}
        for q in pos_react:
            if type_h[q] == 0:
                raise NameError('Undefined reaction type.')

            if type_h[q] == 1:  # k * 1
                prop_dict[q] = react_mat[q, -1]

            if type_h[q] == 2:  # k * nA
                prop_dict[q] = react_mat[q, -1] * int(state[0, reag_numb[q]])

            if type_h[q] == 3:  # k * (nA * nB)
                prop_dict[q] = react_mat[q, -1]
                for e in reag_numb[q]:  # for e in reagent number (index)
                    prop_dict[q] *= int(state[0, e])

            if type_h[q] == 4:  # k * nA(nA - 1)/2
                prop_dict[q] = react_mat[q, -1] / 2
                for e in reag_numb[q]:
                    prop_dict[q] = prop_dict[q] * int(state[0, e]) * (int(state[0, e]) - 1)

        p_sum = sum(prop_dict.values())
        for q in prop_dict:
            if type(q) == int:
                prop_dict[q] = prop_dict[q] / p_sum
        prop_dict['sum'] = p_sum

        return prop_dict

    def pick_reaction(self, props):
        """
        Method for stochastic picking reaction based on uniform distribution and propensities.
        :param props: Dictionary containing individual propensities for every reaction and sum of propensities.
        :return:      Number of the reaction picked
        """

        rand = random.uniform(0, 1)
        global picked
        picked = 0

        props_here = dict((q, props[q]) for q in props if q != 'sum')

        while picked == 0:
            for q in props_here:
                if rand <= props_here[q]:
                    picked = 1
                    return q
                rand = rand - min(props_here.values())

    def calc_delta_t(self, props, time):
        """
        Method for calculating time change based on exponential distribution, where mean is the sum of propensities.
        :param props: Dictionary containing propensities values for each reaction.
        :param time:  Time that has passed since the modulation.
        :return:      Updated time, as a sum of previous time and delta time.
        """

        delta_time = random.expovariate(props['sum']) * 1000  # return ms
        # delta_time = (1/props['sum'])*math.log(1/(random.uniform(0, 1))) * 1000
        time += delta_time
        return time

    def update_state(self, pick):
        """
        Method for updating state based on the reaction picked and previous state.
        :param pick: Number of the reaction picked.
        :return:     Updated state.
        """
        state = self.state
        react_mat = self.react_mat
        state = state + react_mat[pick, :-1]
        return state

    def run(self):
        time = 0
        counter = 0
        m = self.gen_h_matrix()
        h = self.gen_h_list(m)
        rn = self.reag_numbers()

        state_over_time = self.state

        print ("\n Reaction matrix:\n"), (self.react_mat)
        print ("\n Reaction type: \n"), (m)
        print ("\n h: \n"), (h)
        print ("\n reagent number:")

        for k, v in rn.items():
            print ("\t"), (k), ("\t"), (v)

        while time <= self.max_time_x:

            try:
                pr = self.test_if_reaction_possible()
            except NameError:
                print ("\033[1;31m" + "Not full cycle of reactions was completed, "
                                      "consider revising the primary parameters."
                       "\n Number of cycles completed = {}".format(counter) + "\033[0m")
                time = self.max_time_x
                self.time_list.append(time)
                state_over_time = np.vstack([state_over_time, upd])
                sys.stdout = sys.__stdout__
                break

            try:
                p = self.calculate_propensity(h, pr, rn)
            except NameError:
                print ("\033[1;31m" + "Undefined reaction type" + "\033[0m")
                sys.stdout = sys.__stdout__
                break

            time = self.calc_delta_t(p, time)
            self.time_list.append(time)

            # print (counter)
            # print ("\n what reaction is possible \t"), (pr)
            # print (" propensity dictionary \t\t"), (p)
            # print (" new time \t\t\t\t\t"), (time)

            pick = self.pick_reaction(p)
            upd = self.update_state(pick)

            # print (" reaction n picked \t\t\t"), (pick)
            # print (" new state \t\t\t\t\t"), (upd)

            state_over_time = np.vstack([state_over_time, upd])
            self.state = upd
            counter += 1

        output = [[self.time_list], [state_over_time]]
        return output


class GraphPlot(GillespieAdvanced):

    def input(self):
        """
        Redefines __init__ from the Gillespie algorithm main class by taking input from a file.
        """

        if len(sys.argv) > 1:

            fname = 'reactions_data.txt'

            read_file = np.loadtxt(fname, dtype=str)

            self.react_mat = read_file[5:].astype(np.int)
            self.state = np.asmatrix(read_file[3, :-1].astype(np.int))

            self.max_time_x = read_file[0, 2].astype(np.int)
            self.max_species_y = read_file[1, 2].astype(np.int)

        else:
            raise NameError('No reactions are possible.')

    def plot_graph(self):
        """
        Method for plotting graph. Defined x and y axis, species and time respectively. Labels are specified.
        Legend position is assigned.
        :return: Plot
        """
        try:
            self.input()
        except NameError:
            print ("\033[1;31m" + "\n\n<NB!>\tNo file containing reaction data was provided.\t\t<NB!>"
                                  "\n<NB!>\tHence default values were used purely as an example.\t<NB!>" + "\033[0m")
            pass

        gil = self.run()

        y_mol = gil[1][0]
        x_time = gil[0][0]
        plt.figure()
        axes = plt.gca()

        axes.set_xlim(0, self.max_time_x)
        axes.set_ylim(0, self.max_species_y)

        plt.plot(x_time, y_mol[:, 0], 'r', label='Species A')
        plt.plot(x_time, y_mol[:, 1], 'k', label='Species B')
        plt.plot(x_time, y_mol[:, 2], 'y', label='Species C')
        plt.plot(x_time, y_mol[:, 3], 'g', label='Species D')
        plt.plot(x_time, y_mol[:, 4], 'b', label='Species E')
        plt.legend(bbox_to_anchor=(.9, 1), loc=1,
                   ncol=3, borderaxespad=0.)

        plt.show()


def main():
    GraphPlot().plot_graph()

if __name__ == '__main__':
    main()

