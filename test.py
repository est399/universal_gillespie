import unittest
from Gillespie_Universal import *


class TestInput(unittest.TestCase):

    def setUp(self):
        self.state = np.matrix('[10,  10,  10,  0,   0]')

        self.test_reaction_matrix = np.matrix('0    0   -1  0   0   300;'
                                              '-1   0  +1  0   -2   800;'
                                              '0   0   -1  +1  0    400;'
                                              '+2  0   0   0   -1  200')

        self.react_type_mat = np.matrix('0 1 0 0; 1 1 0 1; 1 1 0 0; 0 1 1 0')
        self.react_type_list = [1, 0, 2, 2]
        self.reag_dict = {0: [2], 1: [0, 4], 2: [2], 3: [4]}
        self.pos_react = [0, 2]


class TestS(TestInput):

    def test_everything(self):
        gil = GillespieAdvanced()
        gil.react_mat = self.test_reaction_matrix

        self.assertTrue((gil.gen_h_matrix() == self.react_type_mat).all())
        self.assertEqual(gil.gen_h_list(self.react_type_mat), self.react_type_list)
        self.assertEqual(gil.reag_numbers(), self.reag_dict)

        gil.state = self.state
        self.assertEqual(gil.test_if_reaction_possible(), self.pos_react)

