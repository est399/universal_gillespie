from __future__ import division
import random


class Gillespie:
    """
        Reaction  A + B => AB
        Reverse   AB <= A + B

        Variables:
            kB - binding, kD - dissociating
            a0 - rate of reaction, or the sum of propensities of individual reactions
            nA, nB, nAB - number of molecules
            aB and aD - propensity of binding and dissociation
    """
    kB, kD = 0, 0
    nA, nB, nAB = 0, 0, 0
    time = 0
    props = []
    count = 0
    results = []

    def reaction(self):
        """
        Method for generating statistically correct trajectory of a basic stochastic equation based on Gillespie
        algorithm.
        :return: Writes in stdout number of reaction run, time, direction, propensity, and number of species.
        """
        while self.nA >= 0 and self.nB >= 0 and self.count < self.max_count and self.nAB >= 0:
            self.props = []

            a0 = self.kB * self.nA * self.nB + self.kD * self.nAB
            aB = (self.kB * self.nA * self.nB)/a0
            aD = (self.kD * self.nAB)/a0

            self.props.append([aB, "aB"])
            self.props.append([aD, "aD"])

            rand = random.uniform(0, 1)
            pick = self.pick_reaction(self.props, rand)

            if pick[1] == "aB":
                self.nA -= 1
                self.nB -= 1
                self.nAB += 1
            elif pick[1] == "aD":
                self.nA += 1
                self.nB += 1
                self.nAB -= 1

            delta_time = random.expovariate(a0)*1000                # return ms
            self.time += delta_time
            self.count += 1

            print ("{:3}  t = {:.2f} ms \t{}\t P = {:.3f} \t mol(A) = {:2}; mol(B) = {:2}; mol(AB) = {:2}"
                   .format(self.count, round(self.time, 2), pick[1], round(pick[0], 3), self.nA, self.nB, self.nAB))

    def pick_reaction(self, props, rand):
        """
        Picks reaction based on calculated propensities and randomly generated number.
        :param props:   List of lists containing the propensity value and the name (direction) of the reaction.
        :param rand:    Randomly generated number from uniform (continuous) distribution.
        :return:        List containing the propensity and the name of the reaction picked.
        """
        global picked
        picked = 0
        while picked == 0:
            for q in range(len(props)):
                if rand <= props[q][0]:
                    picked = 1
                    return props[q]
            rand = rand - min(props)[0]

    def run(self, nA, nB, kB, kD, nAB=0, max_count=10):
        self.nA = nA
        self.nB = nB
        self.nAB = nAB
        self.kB = kB
        self.kD = kD
        self.max_count = max_count

        self.reaction()

G = Gillespie()
G.run(10, 10, 900, 400, max_count=100)
